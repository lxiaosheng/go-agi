# Go语言工具辅助库

Go语言是一门伟大、强大的语言，我接触他只有一个多月的时间，已经深刻感受到了他的魅力。

不得不说，Go仍是一门处于发展阶段的语言，尤其是他也是游走于面向函数、面向对象和面向接口编程的边缘（和Scala类似，不过Scala的类和Trait的数量实在让人望而却步）。也包括Go本身提供的很多类库，都存在大量的枚举、反射、hardcode的问题。

所以有了这个工具辅助库，这个库的设计初衷是面向使用，而非做一个大全、或高性能（毕竟Go已经提供了一个环境），我最主要的一个目的就是，将一些日常频繁使用的方法，能用一个统一明确的方式返回结果，少写两个if。

获取指令：`go get git.oschina.net/janpoem/go-agi`

## 工具库

``` go
agi.SplitDirFile(path string) (string, string)
```

根据传入的路径，分离出目录和文件

``` go
agi.RealPathSplitDirFile(path string) (string, string, error)
```

基于真实的物理路径，检查传入的路径，并分离出目录和文件名，这个函数还有待调整。

``` go
agi.Exist(path string) bool
```

检查路径是否存在，这个不会区分是文件还是目录。

``` go
agi.IsFile(path string) bool
```

检查路径是不是文件

``` go
agi.IsDir(path string) bool
```

检查路径是不是目录

``` go
agi.ReadFileByte(path string) ([]byte, error)
```

读取指定的文件，并返回[]byte

``` go
agi.ReadFileStr(path string) (string, error)
```

读取指定的文件，并返回字符串

``` go
agi.TF(format string) (string)
```

生成go的时间格式，主要基于PHP的时间格式，目前支持：`Y-m-d H:i:s`和`Y-n-j H:i:s`

``` go
agi.OffsetIndex(index int, size int) int
```

取得索引值的偏移值，-1对应的是size的最末。

``` go
agi.Seconds(seconds int) time.Duration
```

取得多少秒的间隔时间

``` go
agi.Minutes(minutes int) time.Duration
```

取得多少分钟的间隔时间

``` go
agi.Hours(hours int) time.Duration
```

取得多少小时的间隔时间

## 性能测试

这部分以后会改为一个Benchmark struct。性能测试的代码，主要为了方便对一些写法的性能能快速的做一个甄别，以后会考虑加入CPU和内存的监控。

``` go
agi.TimesTest(times int, fn func(int)) time.Duration
```

循环执行指定的匿名函数次数，并返回执行过程所花费的时间。

## 运行时

这里主要提供一些识别运行时的环境变量，并且这些环境变量不应该可变，所以采用函数的方式来查询。

``` go
agi.Args() []string
```

返回执行当前文件的命令行参数，注意不包含可执行文件本身。这里会后面会加入start-stop-daemon的那个--的判断。

``` go
agi.AppPath() string
```

返回当前执行文件的全路径

``` go
agi.AppDir() string
```

返回当前执行文件的目录

``` go
agi.AppFile() string
```

返回当前执行文件的文件名

``` go
agi.AppTimeZone() string
```

返回当前运行时的时区名称，注意，凡是涉及当前运行时的时区，偏移值的，都是基于启动时，创建一个time.Now()，需要确保你执行环境的系统已经设置了正确的时区（服务器上手第一修正时区，第二upgrade）。

``` go
agi.AppTimeOffset() int
```

取得当前时区的日期偏移值

``` go
agi.AppTimeLoc() *time.Location
```

返回当前时区的Location对象，如果你输出这个对象会不显示任何值（都是私有值），在很多修正时间的地方需要用到。

``` go
agi.GetOsFlag() int
```

取得系统的标示整型`OS_LINUX` => 0, `OS_MAC` => 1, `OS_WIN` => 2, `OS_OTHERS` => 3。

``` go
agi.GetOsEof() string
```

取得系统的EOF

``` go
agi.GetOsDirSpr() string
```

取得系统目录的分隔符。

``` go
agi.GetOsEndian() binary.ByteOrder
```

取得系统的编码顺序，大小编码，你懂的。

## 类型处理

``` go
agi.BytesToStrUnsafe(rawBytes []byte) string
```

[]byte转字符串类型，据说是[黑魔法](http://studygolang.com/articles/2909)


``` go
agi.Round(f float64, places int) string
```

浮点数取精确的小数点数

``` go
agi.StrToFloat(value string) float64
```

字符串数值转浮点，返回的是float64。如果为非数值字符，返回0

``` go
agi.StrToFInt(value string) int
```

字符串数值转整型，不过会先转为浮点，而后再转为整型，尽可能的取得一个有效的值。如果为非数值字符，返回0

``` go
agi.StrToFInt64(value string) int64
```

字符串数值转64位整型，同上

``` go
agi.StrToInt(value string) int
```

这个只是单纯意义的转整型。

``` go
agi.StrToIntBool(value string) bool
```

字符串整值转为布尔类型，这里特别针对0 => false, 大于 0 => true，而不同于go本身提供的ParseBoolean

``` go
agi.StrToTime(value string, layout string) time.Time
```

字符串的时间值，转为时间类型，layout这里是输入go那个奇葩的时间格式：Mon Jan 2 15:04:05 -0700 MST 2006。

为了方便使用，目前提供一个函数`agi.TF("Y-m-d H:i:s")`的方式转为go的时间格式（PHP风格），不过小弟时间有限，没有支持太多的format。

整体调用时：`agi.StrToTime("2014-12-31 12:33:34", agi.TF("Y-m-d H:i:s"))`

``` go
agi.IntToTime(value int64) time.Time
```

字符串的数值转为时间类型，特别注意，如果传入的值 <= 0，也会返回一个时间值，但这个时间类型的值得是无效的。要判断其是否为有效的时间值，需要使用`agi.IsValidTime(agi.IntToTime(value))`。

``` go
agi.IsValidTime(t time.Time) bool
```

检查传入的时间值是不是一个有效的时间值。

``` go
agi.CallAnyStructMethod(v interface{}, method string) interface{}
```

调用任意结构的任意的方法，这个函数还需要修改，需要允许传参数，和返回[]interface{}

``` go
agi.KindOf(v interface {}) reflect.Kind
```

取得任意变量的go原生类型（值反射）。注意，这里获取的是go的原生类型，这是个非常非常重要的方法。如果传入的变量是一个指针，会直接去取指针指向的实际变量。

比如有下面的代码：

``` go
type XYZ struct {
}

type MyInt int

var mya MyInt = 1

agi.KindOf(&XYZ{}) // struct，用于程序等式判断的话，是reflect.Struct
agi.KindOf(mya) // int，用于程序等式判断的话，是reflect.Int
```

``` go
agi.ValueOf(v interface {}) reflect.Value
```

取得任意变量的实际值（值反射），和KindOf一样，如果是一个指针，会去取指针实际指向的变量。

`agi.ValueOf`、`agi.KindOf`、`agi.CallAnyStructMethod`是三个杀手级的方法，go的反射其实蛮完善的，就是……用起来比较费劲，有了这三个函数，能助你立刻去掉那些纷繁的东西，迅速掌握go的反射。让你用起Go语言，比用PHP还简单。

``` go
agi.AnyToInt64(v interface{}) int64
agi.AnyToInt(v interface{}) int
agi.AnyToInt8(v interface{}) int8
agi.AnyToInt16(v interface{}) int16
agi.AnyToUInt8(v interface{}) uint8
agi.AnyToUInt16(v interface{}) uint16
agi.AnyToUInt64(v interface{}) uint64
```

任意值类型，转为整型，目标就是要能涵盖目前go可能出现的变量类型。并且支持，如果是一个结构，尝试检索对象是否具有一个叫做`Int`的方法，并返回该函数的结果。

``` go
agi.AnyToBool(v interface{}) bool
```

任意值类型转布尔类型，如果传入的是数组、切片、map的话，会检查他们的长度是否大于0，大于0就返回true。

``` go
agi.AnyToStr(v interface{}) string
```

任意值类型转字符串类型，如果是一个结构，会尝试执行结构的`String`的方法。

``` go
agi.AnyToFloat(v interface{}) float64
```

任意值类型转float64类型，如果是一个结构，会尝试执行`Float`的方法。

``` go
agi.AnyToRound(v interface{}, places int) float64
```

任意值类型，Round方法，会先转浮点，而后执行Round。


``` go
agi.AnyToTime(v interface{}) time.Time
```

任意值类型，转时间类型，如果是一个结构，会先尝试执行结构的`Time`方法。